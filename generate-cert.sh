#!/usr/bin/env sh


set -eu


: "${CERT_NAME:?Please set the CERT_NAME environment variable}"
: "${DOMAINS:?Please set the DOMAINS environment variable}"
: "${EMAIL:?Please set the EMAIL environment variable}"


echo "Generating certificate ${CERT_NAME} for domains ${DOMAINS}, using ACME server ${ACME_SERVER} in 5 minutes"
sleep 300

certbot certonly \
	--non-interactive \
	--agree-tos \
	--manual \
	--manual-public-ip-logging-ok \
	--manual-auth-hook /acme-dns-auth-hook.sh \
	--preferred-challenges dns \
	--cert-name "${CERT_NAME}" \
	--email "${EMAIL}" \
	--domains "${DOMAINS}" \
	--server "${ACME_SERVER}"


if [[ ! -d "/etc/letsencrypt/live/${CERT_NAME}" ]]; then
	>&2 echo "Certificate ${CERT_NAME} for domains ${DOMAINS} were not generated"
fi
