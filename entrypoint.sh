#!/usr/bin/env sh


set -e


echo "${CRON_PLAN} /generate-cert.sh" | crontab -
exec "$@"
