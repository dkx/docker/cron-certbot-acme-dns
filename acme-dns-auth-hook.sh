#!/usr/bin/env sh


set -eu


: "${ACME_DNS_USER:?Please set the ACME_DNS_USER environment variable}"
: "${ACME_DNS_KEY:?Please set the ACME_DNS_KEY environment variable}"
: "${ACME_DNS_SUBDOMAIN:?Please set the ACME_DNS_SUBDOMAIN environment variable}"
: "${CERTBOT_VALIDATION:?Please set the CERTBOT_VALIDATION environment variable}"


update_with_curl () {
	curl -s -X POST "${ACME_DNS_SERVER}/update" \
		-H "X-Api-User: ${ACME_DNS_USER}" \
		-H "X-Api-Key: ${ACME_DNS_KEY}" \
		--data "{\"subdomain\": \"${ACME_DNS_SUBDOMAIN}\", \"txt\": \"${CERTBOT_VALIDATION}\"}"
}


echo "Adding new TXT record "'"'${CERTBOT_VALIDATION}'"'" to ACME-DNS server ${ACME_DNS_SERVER} for domain "'"'${ACME_DNS_SUBDOMAIN}'"'

RESPONSE=$(update_with_curl)
RESPONSE_HAS_ERROR=$(echo "${RESPONSE}" | jq 'has("error")')

if [[ "${RESPONSE_HAS_ERROR}" == "true" ]]; then
	RESPONSE_ERROR=$(echo "${RESPONSE}" | jq -r '.error')
	>&2 echo "Could not update the TXT record for ${ACME_DNS_SUBDOMAIN}, response error: "'"'${RESPONSE_ERROR}'"'
	exit 1
fi

echo "Successfully updated domain "'"'${ACME_DNS_SUBDOMAIN}'"'" on ACME-DNS server ${ACME_DNS_SERVER}, response: ${RESPONSE}"

sleep 10
