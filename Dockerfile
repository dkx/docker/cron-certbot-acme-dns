FROM alpine
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

ENV ACME_SERVER="https://acme-v02.api.letsencrypt.org/directory"
ENV ACME_DNS_SERVER="https://auth.acme-dns.io"
ENV CRON_PLAN="0 0 * * *"

COPY run.sh /run.sh
COPY entrypoint.sh /entrypoint.sh
COPY generate-cert.sh /generate-cert.sh
COPY acme-dns-auth-hook.sh /acme-dns-auth-hook.sh

RUN apk add --no-cache --update curl certbot ca-certificates jq && \
	mkdir -p /certs && \
	chmod +x /run.sh /entrypoint.sh /generate-cert.sh /acme-dns-auth-hook.sh

VOLUME ["/etc/letsencrypt"]
ENTRYPOINT ["/entrypoint.sh"]
CMD ["/run.sh"]
