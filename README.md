# DKX/Docker/CronCertbotAcmeDns

Docker image for generating LetsEncrypt (wildcard) certificates in cron with 
[joohoi/acme-dns](https://github.com/joohoi/acme-dns).

## docker-compose

```yaml
services:

  certbot:
    image: registry.gitlab.com/dkx/docker/cron-certbot-acme-dns
    volumes:
      - "/certs:/etc/letsencrypt"
    environment:
      - "CERT_NAME=mydomain"
      - "DOMAINS=mydomain.com,*.mydomain.com"
      - "EMAIL=email@mydomain.com"
      - "ACME_DNS_USER=<my-acme-dns-user>"
      - "ACME_DNS_KEY=<acme-dns-key>"
      - "ACME_DNS_SUBDOMAIN=<acme-dns-subdomain>"
      - "CRON_PLAN=0 2 * * *"    # default is 0 0 * * *
```
